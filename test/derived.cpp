#include <cpp_template/derived.hpp>
#include <doctest/doctest.h>

#include <memory>

/*
 * doctest requires an implementation of type to string conversion for regular
 * use. Using double parenthesis avoids decomposition as a workaround. See also:
 * https://github.com/onqtam/doctest/blob/master/doc/markdown/stringification.md
 */

SCENARIO("polymorphic classes")
{
	GIVEN("one class that derives from an abstract base")
	{
		auto der = std::make_shared<::derived>();
		REQUIRE((der != nullptr));

		WHEN("derived class is cast to base")
		{
			auto base = std::dynamic_pointer_cast<::base>(der);

			THEN("the abstract base can be used")
			{
				REQUIRE((base != nullptr));
				base->init();
			}
		}
	}
}
